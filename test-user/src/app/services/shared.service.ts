import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError} from 'rxjs/operators';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http: HttpClient) { }

  
  getUsersData(){
    
    let url = 'https://jsonplaceholder.typicode.com/posts' 
        return this.http
        .get(url)
        .pipe(catchError(e => {
            let data = JSON.parse(e._body);
            if (e.status == 403) {
            }else {
               
            }
            return Observable.throw(e);
        }));
    
    
  }
  getAlbumsData(id: any){
    console.log("value",id) 
    let url = "https://jsonplaceholder.typicode.com/albums/"+id+"/photos"//'https://jsonplaceholder.typicode.com/users/' + id + '/albums' 
        return this.http
        .get(url)
        .pipe(catchError(e => {
            let data = JSON.parse(e._body);
            if (e.status == 403) {
            }else {
               
            }
            return Observable.throw(e);
        }));
    
    
  }
  getPostsData(id: any){
    console.log("value",id)
    let url = "https://jsonplaceholder.typicode.com/users/"+id+"/posts"
        return this.http
        .get(url)
        .pipe(catchError(e => {
            let data = JSON.parse(e._body);
            if (e.status == 403) {
            }else {
               
            }
            return Observable.throw(e);
        }));
    
    
  }
  postPostsData(data: any,userId: any){
      let url ='https://jsonplaceholder.typicode.com/posts/'+userId
      console.log(data)
      return this.http
          .put(url, JSON.stringify(data))
          .pipe(catchError(e => {
              let data = JSON.parse(e._body);
              if (e.status == 403) {
              }else {
                
              }
              return Observable.throw(e);
          }));
  }
  hi(){
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  body: JSON.stringify({
    id: 1,
    title: 'foo',
    body: 'bar',
    userId: 1,
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
})
  .then((response) => response.json())
  .then((json) => console.log(json));
  }
}
