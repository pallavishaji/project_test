import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { MainComponent } from './main.component';
import { AlbumModule } from './modules/album/album.module';
import { PostsModule } from './modules/posts/posts.module';


const routes: Routes = [
    {
      "path": "main",
      "component": MainComponent,
    //   "canActivate": [AuthGuard],
      "children": [
        {
          path: "posts\/:userId",

          loadChildren: () => import('./modules/posts/posts.module').then(m=>m.PostsModule),
        },
        {
          path: "albums\/:userId",

          loadChildren: () => import('./modules/album/album.module').then(m=>m.AlbumModule),
        },
      ]
    },
        {
            "path": "**",
            "redirectTo": "main",
            "pathMatch": "full"
          }
        
        ];
        const routerOptions: ExtraOptions = {
          scrollPositionRestoration: 'top',
          anchorScrolling: 'enabled',
  onSameUrlNavigation: "reload",
  enableTracing: true,
};
@NgModule({
    declarations: [
      
    //   SideBarComponent,
    
    ],
    imports: [
      CommonModule,
      RouterModule.forRoot(routes),
      BrowserModule,
      PostsModule,
            AlbumModule
    ],
    exports: [
     
    //   SiderBarMemberComponent,
    ],
    entryComponents: [
      
  
    ]
  })
  export class MainRoutingModule { }