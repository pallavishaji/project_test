import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  dataUsers: any;
  selectedUserId: any;

  constructor(public service : SharedService,public router:Router) { }


  ngOnInit(): void {
    this.service.getUsersData().subscribe(data => {
      this.dataUsers = data
      console.log("data",this.dataUsers[0])

  })
   
  }
  selectedUser(data: any){
    alert("user selected")
    this.selectedUserId = data
      }
      gotoUrl(name : string){
        console.log(name)

        if(name == 'albums'){
          console.log("here")
          this.router.navigateByUrl('/main/albums/' + this.selectedUserId)

        }else{
          this.router.navigateByUrl('/main/posts/' + this.selectedUserId)

        }

      }
}
