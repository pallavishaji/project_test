import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalManager } from 'ngb-modal';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {
  userId: any;
  routeParam: any;
  albums: any;
  @ViewChild('myModal') myModal:any;
  modalRef: any;
  url: any;

  constructor(public service : SharedService,public activeRoute:ActivatedRoute,private modalService: ModalManager) { }

  ngOnInit(): void {
    this.routeParam = this.activeRoute.snapshot.params;
    this.userId = this.routeParam.userId;
    console.log(this.routeParam) 
    this.service.getAlbumsData(this.userId).subscribe(data => {
      this.albums = data
      console.log("data",this.albums)

  })
  }
  
  selectedAlbum(url : any){
console.log(url)
this.url = url
this.modalRef = this.modalService.open(this.myModal, {
  size: "md",
  modalClass: 'mymodal',
  hideCloseButton: false,
  centered: false,
  backdrop: true,
  animation: true,
  keyboard: false,
  closeOnOutsideClick: true,
  backdropClass: "modal-backdrop"
})
  }
  closeModal(){
    this.modalService.close(this.modalRef);
    //or this.modalRef.close();
}
}
