import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlbumComponent } from './album.component';
import { RouterModule, Routes } from '@angular/router';
import { ModalModule } from 'ngb-modal';

const routes: Routes = [
  {
      
              'path': '',
              'component': AlbumComponent,
     
  },
];
console.log("album module")
@NgModule({
  declarations: [
    AlbumComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ModalModule
  ]
})
export class AlbumModule { }
