import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsComponent } from './posts.component';
import { RouterModule, Routes } from '@angular/router';
import { ModalModule } from 'ngb-modal';
import { FormsModule, NgModel } from '@angular/forms';
const routes: Routes = [
  {
      
              'path': '',
              'component': PostsComponent,
     
  },
];


@NgModule({
  declarations: [
    PostsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ModalModule,
    FormsModule 
  ]
})
export class PostsModule { }
