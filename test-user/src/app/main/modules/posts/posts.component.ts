import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalManager } from 'ngb-modal';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts: any;
  routeParam: any;
  userId: any;
  body: any;
  modalRef: any;
  @ViewChild('myModal') myModal:any;
  id: any;
  title: any;
  userid: any;

  constructor(public service : SharedService,public activeRoute:ActivatedRoute,private modalService: ModalManager) { }

  ngOnInit(): void {
    this.routeParam = this.activeRoute.snapshot.params;
    this.userId = this.routeParam.userId;
    console.log(this.routeParam) 
    this.service.getPostsData(this.userId).subscribe(data => {
      this.posts = data
      console.log("data",this.posts)
  
})
  }
  selectedPost(data : any){
    console.log(data)
    this.body = data.body
    this.id = data.id
    this.title = data.title
    this.userid = data.userId
    this.modalRef = this.modalService.open(this.myModal, {
      size: "md",
      modalClass: 'mymodal',
      hideCloseButton: false,
      centered: false,
      backdrop: true,
      animation: true,
      keyboard: false,
      closeOnOutsideClick: true,
      backdropClass: "modal-backdrop"
    })
      }
      closeModal(){
        this.modalService.close(this.modalRef);
        //or this.modalRef.close();
    }
    updatePosts(){
      this.service.hi()
      let data =
      
      {
        id: this.id,
        title: this.title,
        body: this.body,
        userId: this.userid
      }
      this.service.postPostsData(data,this.userId).subscribe(res => {
        
alert(res)    
  })
    }
}
